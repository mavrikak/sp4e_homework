#include "compute_temperature.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "csv_reader.hh"
#include "my_types.hh"
#include <gtest/gtest.h>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iostream>

/* ------------------------------------------------------------------------- */

// Exercise 4, Part 2
TEST(ComputeTemperature, compute_hom) {

    // Initial system from test data file
    std::vector<MaterialPoint> initial;

    // Initial state reading
    std::string filename = "../../src/test_data_hom.txt";
    std::ifstream is(filename.c_str());
    std::string line;

    if (is.is_open() == false) {
        std::cerr << "Cannot open file " << filename << std::endl;
        throw;
    }
    
    while (is.good()) {
        getline(is, line);

        if (line[0] == '#' || line.size() == 0)
        continue;

        MaterialPoint p;
        std::stringstream sstr(line);
        p.initself(sstr);
        initial.push_back(p);
    }
    is.close();

    // System evolution
    UInt nsteps = 4;
    UInt freq = nsteps + 1;
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol = factory.createSimulation(filename, 1.);
    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    evol.evolve();

    // Testing if final and initial states are the same
    UInt nb_particles = evol.getSystem().getNbParticles();
    for (UInt p = 0; p < nb_particles; ++p) {
        auto mpf = dynamic_cast<MaterialPoint&>(evol.getSystem().getParticle(p));
        ASSERT_NEAR(initial[p].getTemperature(), mpf.getTemperature(), 1e-10);
    }
}

/* ------------------------------------------------------------------------- */

// Exercise 4, Part 3
TEST(ComputeTemperature, compute_sin) {

    // Initial system from test data file
    std::vector<MaterialPoint> initial;

    // Initial state reading
    std::string filename = "../../src/test_data_sin.txt";
    std::ifstream is(filename.c_str());
    std::string line;

    if (is.is_open() == false) {
        std::cerr << "Cannot open file " << filename << std::endl;
        throw;
    }
    
    while (is.good()) {
        getline(is, line);

        if (line[0] == '#' || line.size() == 0)
        continue;

        MaterialPoint p;
        std::stringstream sstr(line);
        p.initself(sstr);
        initial.push_back(p);
    }
    is.close();

    // System evolution
    UInt nsteps = 4;
    UInt freq = nsteps + 1;
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol = factory.createSimulation(filename, 1.);
    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    evol.evolve();

    // Testing if final and initial states are the same
    UInt nb_particles = evol.getSystem().getNbParticles();
    for (UInt p = 0; p < nb_particles; ++p) {
        auto mpf = dynamic_cast<MaterialPoint&>(evol.getSystem().getParticle(p));
        ASSERT_NEAR(initial[p].getTemperature(), mpf.getTemperature(), 1e-10);
    }
}

/* ------------------------------------------------------------------------- */

// Exercise 4, Part 4 (Optional) 
TEST(ComputeTemperature, compute_tri) {

    // Initial system from test data file
    std::vector<MaterialPoint> initial;

    // Initial state reading
    std::string filename = "../../src/test_data_tri.txt";
    std::ifstream is(filename.c_str());
    std::string line;

    if (is.is_open() == false) {
        std::cerr << "Cannot open file " << filename << std::endl;
        throw;
    }
    
    while (is.good()) {
        getline(is, line);

        if (line[0] == '#' || line.size() == 0)
        continue;

        MaterialPoint p;
        std::stringstream sstr(line);
        p.initself(sstr);
        initial.push_back(p);
    }
    is.close();

    // System evolution
    UInt nsteps = 4;
    UInt freq = nsteps + 1;
    MaterialPointsFactory::getInstance();
    ParticlesFactoryInterface& factory = ParticlesFactoryInterface::getInstance();
    SystemEvolution& evol = factory.createSimulation(filename, 1.);
    evol.setNSteps(nsteps);
    evol.setDumpFreq(freq);
    evol.evolve();

    // Testing if final and initial states are the same
    UInt nb_particles = evol.getSystem().getNbParticles();
    for (UInt p = 0; p < nb_particles; ++p) {
        auto mpf = dynamic_cast<MaterialPoint&>(evol.getSystem().getParticle(p));
        ASSERT_NEAR(initial[p].getTemperature(), mpf.getTemperature(), 1e-10);
    }
}

/* ------------------------------------------------------------------------- */
