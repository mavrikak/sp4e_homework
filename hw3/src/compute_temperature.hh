#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute temperature of material points
class ComputeTemperature : public Compute {

  // Virtual implementation
public:
  //! Temperature computation
  void compute(System& system) override;

  // Constructors/Destructors
public:
  ComputeTemperature(Real dt);
  
  // Accessors
public:
  //! set the mass density
  void setRho(Real rho);
  //! set the specific heat capacity
  void setC(Real C);
  //! set the heat conductivity
  void setKappa(Real k);
  //! set the time step
  void setDeltat(Real dt);
  
  // Members
private:
  //! Mass density
  Real rho = 1.;
  //! Specific heat capacity
  Real C = 1.;
  //! Heat conductivity
  Real k = 1.;
  //! Time step
  Real dt;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
