#ifndef FFT_HH
#define FFT_HH

/* ------------------------------------------------------------------------- */

#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>

/* ------------------------------------------------------------------------- */

//! structure for fast fourier transform  
struct FFT {

  //! forward FFT 
  static Matrix<complex> transform(Matrix<complex>& m);
  //! backward (or inverse) FFT
  static Matrix<complex> itransform(Matrix<complex>& m);
  //! FFT frequencies 
  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------------------------- */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  // matrix initialization
  UInt ndim = m_in.size();
  Matrix<complex> fft = Matrix<complex>(ndim);

  // DFT implementation
  fftw_plan plan = fftw_plan_dft_2d(ndim, ndim, (fftw_complex*)m_in.data(),
                                    (fftw_complex*)fft.data(), FFTW_FORWARD,
                                    FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);

  return fft;
}

/* ------------------------------------------------------------------------- */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  // matrix initialization
  UInt ndim = m_in.size();
  Matrix<complex> ifft = Matrix<complex>(ndim);

  // inverse DFT implementation
  fftw_plan plan = fftw_plan_dft_2d(ndim, ndim, (fftw_complex*)m_in.data(),
                                    (fftw_complex*)ifft.data(), FFTW_BACKWARD,
                                    FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);

  ifft /= ndim * ndim;

  return ifft;
}

/* ------------------------------------------------------------------------- */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {

    Matrix<std::complex<int>> Freq(size); 

    // define the point at which we turn to negative values of the frequency
    int bound = size/2 - (size -1)%2;

    // temporary values to be stored permanently as a complex number every time
    int valx, valy;
    // variable tempx has the value of the lowest frequency 
    int tempx = - size/2 -1; 
    for( int i = 0; i < size; i++ ) {
        int tempy = - size/2 ; 
        if ( i > bound )    tempx += 1;
        for( int j = 0; j < size; j++ ) {
            valx = i;
            valy = j;
            if ( i > bound) {
                valx = tempx;
            }
            if (j > bound) { 
                valy = tempy; 
                tempy += 1;
            }
            std::complex<int> z(valx, valy);
            Freq(i, j) = z;  
        };      
    };

    return Freq;

}

/* ------------------------------------------------------------------------ */

#endif  // FFT_HH
