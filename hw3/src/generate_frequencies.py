#!/usr/bin/python3

import numpy as np 

ndim = 512 
# generate frequencies using fft package of numpy 
# multiply by ndim to get the integer frequencies
f =  np.array( np.fft.fftfreq( ndim ) * ndim ) 

# naively create the 2D matrix of (qx,qy) 
fs = []
for i in f:
    for j in f:
        fs.append((i,j))

# save in a text file 
np.savetxt( "./frequencies.txt", fs, fmt='%.0f')

