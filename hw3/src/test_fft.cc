#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>
#include <fstream>
#include <sstream>

/* ------------------------------------------------------------------------- */

TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}

/* ------------------------------------------------------------------------- */

TEST(FFT, inverse_transform) {
  // NxN matrix
  UInt N = 512;
  Matrix<complex> m(N);

  // matrix with the DFT of cos(k*i)
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (i == 1 && j == 0)
      val = N * N / 2;
    else if (i == N - 1 && j == 0)
      val = N * N / 2;
    else
      val = 0;
  }

  // inverse DFT
  Matrix<complex> res = FFT::itransform(m);

  // checking IDFT[ DFT[ cos(k*i) ] ] with cos(k*i)
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);    
    ASSERT_NEAR(std::real(val), cos(k * i), 1e-10);
  }
}

/* ------------------------------------------------------------------------- */

TEST(FFT, compute_frequencies) {

    UInt N = 512;
    Matrix<std::complex<int>> m(N); 

    // load frequencies generated with python 
    std::ifstream file; 
    file.open( "../../src/frequencies.txt" );
    
    int a, b; 
    int i = 0, j = 0;
    while ( file >> a >> b ) {
        std::complex<int> z(a,b);
        m(i, j) = z;
        if ( j == N-1) { i++; j = 0; }
        else j++;
    }
    file.close();
    
    // compute frequencies with custom class
    Matrix<std::complex<int>> res = FFT::computeFrequencies(N);

    // compare values 
    for (auto&& entry : index(res) ) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        ASSERT_NEAR(std::real(val), m(i, j).real(), 1e-10);  
        ASSERT_NEAR(std::imag(val), m(i, j).imag(), 1e-10);  
    }

}

/* ------------------------------------------------------------------------- */
