#!/usr/bin/python3

# ===================================================================

import math
import numpy as np

# ===================================================================

def create_data(grid_size: int, temp: callable, heat: callable, L: int):
    # by default, assumes a symmetric square boundary about 0 in both
    # directions
    material_points = []
    field_names = ['Position_X', 'Position_Y', 'Position_Z',
                   'Velocity_X', 'Velocity_Y', 'Velocity_Z',
                   'Force_X', 'Force_Y', 'Force_Z',
                   'Mass', 'Temperature', 'Heat']
   
    xr = np.linspace( -L//2, L//2, grid_size)
    yr = np.flip( np.linspace( -L//2, L//2, grid_size) )
    # Loop through each x and y coordinate in the grid
    for x in xr:
        for y in yr:
            # Create a particle at the current (x, y) position, z = 0,
            # zero velocity, zero force and mass 1
            # temperature and heat defined by callables temp, heat
            particle = [x,y,0.0] + [0.0]*6 + [1.0,temp(x,y),heat(x,y)] 
            material_points.append(particle)
    return field_names, material_points


# =============================================================================

if __name__ == "__main__":

    # Specify the grid size (number of material points per side)
    grid_size = 100
    # the length of each size of the square boundary
    L = 2 

    # TEST 1 : Exercise 4, Part 2 
    temp = lambda x, y: 40.0 
    heat = lambda x, y: 0.0 
    fname, material_points = create_data(grid_size, temp=temp, heat=heat, L=L)
    np.savetxt('test_data_hom.txt', material_points, fmt='%.12f', delimiter=' ',
               header=' '.join(fname) )   

    # TEST 2 : Exercise 4, Part 3 
    k = 2 * math.pi / L
    temp = lambda x, y: math.sin( k * x  )
    heat = lambda x, y: k**2.0 * math.sin( k * x )  
    fname, material_points = create_data(grid_size, temp=temp, heat=heat, L=L)
    np.savetxt('test_data_sin.txt', material_points, fmt='%.12f', delimiter=' ',
               header=' '.join(fname) )   

    # TEST 3 : Exercise 4, Part 4 (Optional) 
    temp = lambda x, y: ( -x-1 if x<= -0.5 
           else ( x if (x > -0.5 and x <= 0.5) else -x + 1) )  
    heat = lambda x, y: ( 1 if x == 0.5 
           else ( -1 if x == -0.5 else 0) ) 
    fname, material_points = create_data(grid_size, temp=temp, heat=heat, L=L)
    np.savetxt('test_data_tri.txt', material_points, fmt='%.12f', delimiter=' ',
               header=' '.join(fname) )   

# =============================================================================
