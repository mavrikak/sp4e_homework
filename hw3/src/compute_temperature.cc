#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

ComputeTemperature::ComputeTemperature(Real dt) : dt(dt) {}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setRho(Real rho) { this->rho = rho; }

void ComputeTemperature::setC(Real C) { this->C = C; }

void ComputeTemperature::setKappa(Real k) { this->k = k; }

void ComputeTemperature::setDeltat(Real dt) { this->dt = dt; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {
    // Material points' matrices
    UInt size = system.getNbParticles();    // number of particles
    UInt ndim = std::sqrt(size);            // square matrix dimension
    Matrix<complex> temp(ndim);             // temperature
    Matrix<complex> heat(ndim);             // volumetric heat source

    // Volumetric heat source of the problem
    for (auto&& entry : index(heat)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        auto p = dynamic_cast<MaterialPoint&>(system.getParticle(i * ndim + j));
        val = static_cast<complex>(p.getHeatRate());
    }
    
    // Current temperatures of the problem
    for (auto&& entry : index(temp)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        auto p = dynamic_cast<MaterialPoint&>(system.getParticle(i * ndim + j));
        val = static_cast<complex>(p.getTemperature());
    }

    // DFT
    Matrix<complex> temp_DFT = FFT::transform(temp);
    Matrix<complex> heat_DFT = FFT::transform(heat);
    Matrix<std::complex<int>> freqs = FFT::computeFrequencies(ndim);

    // DFT domain solution matrix
    Matrix<complex> sol_DFT(ndim);
    
    // DFT domain solve
    for (auto&& entry : index(sol_DFT)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        Real qx2 = 4 * M_PI * std::imag(freqs(i,j)) *
                              std::imag(freqs(i,j)) / size;
        Real qy2 = 4 * M_PI * std::real(freqs(i,j)) *
                              std::real(freqs(i,j)) / size;
        val = (heat_DFT(i,j) - k * temp_DFT(i,j) * (qx2 + qy2))/(rho * C);
    }

    // Inverse DFT (solution)
    Matrix<complex> sol = FFT::itransform(sol_DFT);

    // Solution update
    for (auto&& entry : index(sol)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto p = dynamic_cast<MaterialPoint&>(system.getParticle(i * ndim + j));
        p.getTemperature() += dt * std::real(sol(i,j));
    }
}

/* -------------------------------------------------------------------------- */
