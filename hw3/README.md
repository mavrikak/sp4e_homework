# Heat Solver Implementation (Assignment 3)

## PhD Students 

Parmenion Mavrikakis - Nanophotonics and Metrology Laboratory, EPFL  
Stavros Athanasiou - Nanophotonics and Metrology Laboratory, EPFL 

## build/ directory

First step is to create a `build/` directory and we should do that in `hw3/` 
as follows:

```console 
$ mkdir build/
```

## Dependencies 

### Google test 

For google test, run: 
  
```console
$ git submodule update --init
```

### FFT

For fast Fourier tranform in C++, `fftw3` library is required. 
By default it is enabled in `src/CMakeLists.txt`. 
To disable FFT, use the option `ENABLE_FFT` in the `src/CMakeLists.txt`.  
Alternatively, run the following in `build/`.

```console 
$ cmake -D ENABLE_FFT=OFF ..
```

If we want to enable FFT again, run: 
 
```console 
$ cmake -D ENABLE_FFT=ON ..
```

## How to run 

### make command

Inside the `build/` directory run the following to make all tests: 

```console
$ cmake .. 
$ make test 
```

If you want to run each test separately, then run the following commands in `build/src/`:

```console
$ ./test_fft 
$ ./test_heat 
$ ./test_kepler 
```

**Note**: The above commands should be run within the indicated directories 
due to path conflicts. 

### Generate test frequencies with Python

To generate frequencies with python, we have created a small script called
`src/generate_frequencies.py` which uses numpy.fft.fftfreq. 
The frequencies are then stored in a text file which is readable by the 
corresponding executable `test_fft`. 
Inside `hw3/src/` you can run: 

```console
$ python3 generate_frequencies.py 
```        

### Generate material points data with Python

To generate the heat source and initial temperature configuration data for the 
heat equation tests, you can run in `hw3/src/` the following command:

```console  
$ python3 test_heat_generate_data.py 
``` 

which produces text files in the `src/` directory, to be loaded by `test_heat`.  

### How to launch simulations for Paraview

If you have only tested before, then in `build/` you have to

```console
$ make
```

Inside `build/src/`, run `particles` command which is the executable for 
`src/main.cc`. 
Provide the required arguments and run the simulation, which stores the 
output in `dumps/`.  
The output files can be used in Paraview to view the evolution of the system.
Provided example: 

```console
$ ./particles 100 10 ../../src/test_data_hom.txt material_point 0.1
```

In this example, we use the data generated for the tests.

## Description of additional Classes

In the provided code for the third homework there were some new C++ classes, which are:

* `MaterialPoint`
* `Matrix`
* `MaterialPointsFactory`
* `fft: The FFTW wrapping interface`

### Material Point

Files: `material_point.hh`, `material_point.cc`

* Derived class from the Particle base class with public type of inheritance.
* Additional members: `temperature`, `heat_rate`.
* Additional accessors: `getTemperature()`, `getHeatRate()`, which also can be used to modify the members, since they return a reference.
* Polymorhism:
    * `initself(std::istream& sstr)`: Initialization of the members with the help of an input stream.
    * `printself(std::ostream& stream)`: Output of the values of the members.

This class is used to create instances of material points. Thus, it consists the base of the code, since the material points are essential regarding the implementation of the heat equation solver.

### Matrix

* `MatrixIterator`:
    * Custom iterator for the elements of a square matrix (next class).
    * Members: current index, total size of the matrix, and a pointer to the matrix data.

* `Matrix`:
    * Template class of a square matrix.
    * It has a vector storage to keep the matrix elements.
    * Provides methods for: 
        * resizing: `resize(UInt size)`,
        * accessing elements: `operator()(UInt i, UInt j)`,
        * element-wise division by a constant.
    * It has iterators by using the begin and end methods.

* `MatrixIndexIterator`
    * Derived class from `MatrixIterator`
    * Provides an additional operation that returns a tuple containing row, column, and reference to the matrix element.

* `IndexedxMatrix`:
    * Member: Matrix reference
    * Provides iterators for indexed access (row, column, element).
* `index`: Utility function to create an IndexedMatrix from a regular Matrix.
* `iterator_traits`: Specializes the `std::iterator_traits` template for `MatrixIterator`, providing the `value_type` information.

This file gives the capability to use square matrices. Hence, FFT and Inverse FFT can be implemented and also grids of material points can be manipulated, for the creation of a heat equation solver.

### Material Points Factory

Factory class for creating instances of material points

* Inherits from `ParticlesFactoryInterface` pure virtual class.
* Private default constructor, so that instances of this class cannot be created freely (Singleton design pattern).
* `createSimulation`: Overrides the corresponding method from `ParticlesFactoryInterface` to create a simulation for the heat problem based on a given filename and timestep.
* `createParticle`: Overrides the corresponding method from `ParticlesFactoryInterface` to create a unique pointer of type Particle for a `MaterialPoint`.
* `getInstance`: Static method to get an instance of the factory. It follows the Singleton pattern.

Factory class for creating material points and providing methods for creating simulations for the heat problem. The use of a private constructor and a static method for obtaining an instance shows that it follows the Singleton pattern. Hence, the arrangement and the organization of sets of MaterialPoint objects follows the same principles as other types of particles.

### FFT

Fast Fourier Transform (FFT) implementation using the FFTW library

* Contains static methods for performing FFT and inverse FFT transformations on a matrix of complex numbers.
* Includes a method named computeFrequencies for generating a square matrix containing frequencies.
* `transform(Matrix<complex>& m_in)`: Performs the forward FFT transformation using FFTW. It initializes a matrix for the result and executes the FFT using FFTW plans.
* `itransform(Matrix<complex>& m_in)`: Performs the inverse FFT transformation using FFTW. Similar to transform, it initializes a matrix for the result, executes the inverse FFT, and then normalizes the result.
* `computeFrequencies(int size)`: Generates a square matrix containing frequencies. It uses nested loops to fill the matrix with complex integer values based on the given size.

FFT is vital for computations that lead to the final solution of the problem, so it enables an easy way of implementing the heat solver.
