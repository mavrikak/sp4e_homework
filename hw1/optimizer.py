
# =============================================================================

import numpy as np
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres

# =============================================================================

def _minimization(A: np.array, b: np.array):
    """

    Computes the solution of the linear equation Ax = b, using either the BFGS
    or LGMRES methods. 

    Arguments:
    ----------
    A : np.array
        (n x n)  matrix  
    b : np.array
        (n x 1) column vector 

    Returns:
    --------
    optim : callable
        the function responsible for the optimization - the optimizer

    """
    
    # starting point : zeros
    x0 = np.zeros( b.shape[0] ) 

    # a lambda function for S(x) - used for lgmres 
    S = lambda x: 0.5 * np.dot(x, np.dot(A, x)) - np.dot(x, b)

    # list to save intermediate values of x
    vals_ls = [] 

    def callback(x: np.array):
        """
        This function collects the intermediate values of x and S(x) until
        convergence.

        Arguments:
        ----------
        x : np.array
            intermediate value of Ax = b

        """
        vals_ls.append( x.tolist() + S(x).tolist()  )


    def optim(method: str = 'BFGS'):
        """
        Performs the optimization using the chosen method. 

        Arguments:
        ----------
        method: str     (default: BFGS) 
            the optimization method / options: BFGS, LGMRES

        Returns:
        --------
        vals : np.array 
            contains the points until convergence (including the solution), 
            each row contains the vector x and S(x) (last element)

        """
        
        # BFGS method implementation
        if method == 'BFGS':

            # save the initial point - it is not included by the optimizer
            vals_ls.append( x0.tolist() + S(x0).tolist()  )

            # BFGS method with inputs: S = quadratic lambda function S(x),
            # initial solution: x0 = [0., 0.], method = BFGS
            minimize(S, x0, method=method, tol=1e-9, callback=callback)

        # LGMRES method implementation
        elif method == 'LGMRES':

            # lgmres method with inputs matrices A and b
            # initial point: x0 = [0., 0.]
            lgmres(A, b, x0, tol=1e-9, callback=callback)

        # raise error if method is not BFGS or LGMRES
        else:
            raise ValueError("Invalid method. Use 'BFGS' or 'LGMRES'")

        vals = np.array( vals_ls.copy() )
        # clear the list (that stores intermediate values) for next 
        # call of optim 
        vals_ls.clear() 
        return vals 
    
    return optim 


# =============================================================================
