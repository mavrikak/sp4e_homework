
# =============================================================================

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# =============================================================================

def _visualize(S: callable, sol: np.array, plot_label: str =''):
    """

    Plot the 2D surface of S(x). Only applies to 2D parameter space. 
    
    Arguments:
    ----------
    S : callable
        the 2D surface 
    sol : np.array
        array of intermediate points until convergence (as obtained from
        either _minimization or _gmres modules) 
    plot_label : str    (default: '')
        the name of the method to be included in the plot title

    """

    # raise error if parameter space is > 2 or < 2
    if len(sol[-1,0:-1]) != 2: 
        raise AssertionError("Visualization is valid for a 2D linear system.")

    # create a mesh grid for the 3D surface plot
    x1_range = np.linspace(-3, 3, 100)
    x2_range = np.linspace(-3, 3, 100)
    X1, X2 = np.meshgrid(x1_range, x2_range)
    Sl = np.array([[S(np.array([x, y])) for x in x1_range] for y in x2_range])
    Sl = Sl.squeeze() 

    # create the figure 
    fig = plt.figure( figsize=(12,6) )
    ax1 = fig.add_subplot(111, projection='3d')

    # Plot the 3D surface
    ax1.plot_surface(X1, X2, Sl, cmap='viridis', alpha=0.8, 
                     lw=0.5, rstride=8, cstride=8)

    # plot the solutions trajectory on the 3D surface
    ax1.plot(sol[:,0], sol[:,1], sol[:,2], marker='o', linestyle='-', 
             color='r')

    # add plot title and axes labels 
    ax1.set_title( plot_label, fontsize=18 )
    ax1.set_xlabel( r'$x_1$', fontsize=15)
    ax1.set_ylabel( r'$x_2$', fontsize=15 )
    ax1.set_zlabel( r'$S(x)$', fontsize=15)

    plt.show()

# =============================================================================

