
# =============================================================================

import numpy as np

# =============================================================================

def _gmres(A: np.array, b: np.array, it_max: int, thresh: float):
    """

    Applies the GMRES method.  

    Arguments:
    ----------
    A : np.array
        (n x n) matrix 
    b : np.array
        (n x 1) column vector
    it_max : int 
        maximum iterations
    thresh : float 
        threshold set for convergence
    
    Returns:
    --------
    vals : np.array
        the intermediate and final values of the optimization problem
        (including S) - each row contains the x and S(x) 

    """
    
    # S(x) lambda function
    S = lambda x: ( (0.5 * np.einsum('k,k->', x, np.einsum('ik,k->i', A, x)) - 
                    np.einsum('k,k->', x, b)).reshape((1,)) )

    # Reshaping for residual calculation
    b = np.reshape(b, (b.shape[0],))

    # set starting point
    x0 = np.zeros(b.shape[0])

    # Residual: r0 = b - A*x0
    r = b - np.einsum('ik,k->i', A, x0)

    # Givens rotation matrices
    sn = np.zeros(it_max)
    cs = np.zeros(it_max)

    # First vector in the standard basis
    e1 = np.zeros(it_max+1)
    e1[0] = 1

    # Matrix of orthonormal vectors q (first is q0)
    Q = np.zeros( (len(b), it_max + 1) )
    Q[:, 0] = r / np.sqrt( np.einsum( 'k, k->', r, r ) )

    # Hessenberg matrix
    H = np.zeros( (it_max + 1, it_max) )
    
    # beta = ||r0|| * e1
    beta = np.sqrt( np.einsum( 'k, k->', r, r ) ) * e1

    vals_ls = []

    # GMRES' iterations
    for k in range(it_max):

        # Arnoldi iteration function
        H[:k+2, k], Q[:, k+1] = _arnoldi_it(A, Q, k)
        
        # Givens rotation function
        H[:k+2, k], cs[k], sn[k] = _givens_rot(H[:k+2, k], cs, sn, k)
        
        # New beta vector (new residual)
        beta[k + 1] = -sn[k] * beta[k]
        beta[k] = cs[k] * beta[k]

        # New error
        error = np.abs( beta[k + 1] ) / np.einsum( 'k,k->', b, b )

        # y_n that minimizes ||r_n||
        y = np.einsum('ik,k->i', np.linalg.inv( H[:k+1, :k+1] ), beta[:k+1])

        # x_n = x0 + Q_n*y_n
        x = x0 + np.einsum( 'ik,k->i', Q[:,:k+1], y )
        vals_ls.append( x.tolist() + S(x).tolist() ) 

        # Check threshold
        if error <= thresh:
            break
    
    vals = np.array( vals_ls )
    return vals, error

# =============================================================================

def _arnoldi_it(A: np.array, Q: np.array, k: int):
    """

    Applies the Arnoldi iteration method.

    Arguments:
    ----------
    A : np.array
        (n x n)  matrix 
    Q : np.array 
        (m x n) matrix 
    k : int 
        number of orthonormal vector / number of GMRES iteration 

    Returns:
    --------
    h : np.array
        Hessenberg matrix 
    q : np.array
        orthonormal vector

    """

    # New q vector (part of basis for Krylov subspace)
    q = np.einsum('ik, k->i', A, Q[:, k])

    # Part of Hessenberg matrix
    h = np.zeros(k + 2)

    # Projections on previous vectors removal
    for i in range(k):
        h[i] = np.einsum('k,k->', q, Q[:, i])
        q = q - h[i] * Q[:, i]
    
    # Insert new vector
    h[k + 1] = np.sqrt(np.einsum('k,k->', q, q))
    q = q / h[k + 1]

    return h, q

# =============================================================================

def _givens_rot(h, cs, sn, k):
    """

    Applies the Givens rotation (to be used in the GMRES loop) 

    """

    # Givens rotation matrix
    def rotation(v1, v2):
        t = np.sqrt(v1**2 + v2**2)
        cs = v1 / t
        sn = v2 / t
        return cs, sn

    for i in range(k):
        temp = cs[i] * h[i] + sn[i] * h[i + 1]
        h[i + 1] = -sn[i] * h[i] + cs[i] * h[i + 1]
        h[i] = temp

    # cos & sin rotation values
    cs_k, sn_k = rotation(h[k], h[k + 1])

    # Eliminate H(i + 1, i)
    h[k] = cs_k * h[k] + sn_k * h[k + 1]
    h[k + 1] = 0
    return h, cs_k, sn_k

# =============================================================================
