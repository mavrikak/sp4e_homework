#!/usr/bin/python3

# =============================================================================

import numpy as np
from optimizer import _minimization
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from visualization import _visualize
from GMRES import _gmres 

# =============================================================================

msg = 'Optimization methods'
parser = ArgumentParser(description=msg, 
                        formatter_class=ArgumentDefaultsHelpFormatter)

parser.add_argument('-A', '--matA', type=float, nargs='+', required=True,
                    metavar='', help='Provide matrix A', )
parser.add_argument('-b', '--matb', type=float, nargs='+', required=True,
                    metavar='', help='Provide column vector b')
parser.add_argument('-m', '--method', type=str, default='GMRES', metavar='', 
                    help='Choose a method: BFGS, LGMRES, GMRES (custom)') 
parser.add_argument('-p', '--plot', action='store_true',
                    help='Plot 3D surface and solutions trajectory')
args = parser.parse_args()

# =============================================================================

if __name__ == '__main__':
    
    # creating arrays A, b
    lenA, lenB = len( args.matA ), len( args.matb )
    A = np.array( args.matA ).reshape( (int(lenA**(1/2)), int(lenA**(1/2))) )
    b = np.array( args.matb ).reshape( (lenB, 1) )
    S = lambda x: 0.5 * np.dot(x, np.dot(A, x)) - np.dot(x, b)

    if args.method == 'BFGS': 

        # initialize optimizer
        optimizer = _minimization(A, b)

        # Solve the linear system using the 'BFGS' method
        sol = optimizer( method='BFGS' )
    
    elif args.method == 'LGMRES':
        
        # initialize optimizer
        optimizer = _minimization(A, b)

        # Solve the linear system using the 'lgmres' method
        sol = optimizer( method='LGMRES' )
    
    elif args.method == 'GMRES':
    
        # Solve the linear system using the 'lgmres' method
        sol, _ = _gmres(A, b, it_max=100, thresh=1e-9)

    else:

        # raise error if another method is given
        raise ValueError("Invalid method. Use 'BFGS', 'LGMRES' or 'GMRES'.")

    # print result 
    to_print = f'x = {sol[-1,0:-1]}, S(x) = {sol[-1,-1]}'
    print(f"Solution using {args.method}:", to_print)

    if args.plot: 
        _visualize(S, sol, 
                   plot_label=f'{args.method}: 3D Surface and Trajectory')

    
# =============================================================================
