# Optimization methods (Assignment 1) 

## PhD Students 

Parmenion Mavrikakis - Nanophotonics and Metrology Laboratory, EPFL  
Stavros Athanasiou - Nanophotonics and Metrology Laboratory, EPFL 

## Dependencies 

| Library | Specific Imports |
| ----------- | ----------- |
| numpy | - |
| argparse | ArgumentParser,<br> ArgumentDefaultsHelpFormatter |
| scipy.optimize | minimize |
| scipy.sparse.linalg | lgmres |
| mpl_toolkits.mplot3d | Axes3D |
| matplotlib.pyplot | - |

## Structure 

| File Names | Functions |
| ----------- | ----------- |
| main.py | main function |
| optimizer.py | _minimization |
| visualization.py | _visualize |
| GMRES.py | _gmres |

### Functions

**_minimization**: Computes the solution to the linear equation Ax = b, using either the BFGS or LGMRES methods. 

**_visualize**: Visualization of the two-dimensional surface (applies only for 2D linear systems)
    
**_gmres**: Applies the GMRES method.

## How to run 

### Command line arguments

* -h: Show help message and exit
* -A: Provide square matrix A of the linear system Ax = b (i.e. -A 8 1 1 3)
* -b: Provide vector b of the linear system Ax = b (i.e. -b 2 4)
* -m: Choose a method to run: BFGS, LGMRES, GMRES (custom made)
* -p: Plot 3D surface and solutions trajectory (omit if you don't want to plot)

### Run the BFGS optimizer and plot the result (Exercise 1)

python3 main.py -A 8 1 1 3 -b 2 4 -m BFGS -p

### Run the LGMRES optimizer and plot the result (Exercise 1)

python3 main.py -A 8 1 1 3 -b 2 4 -m LGMRES -p

### Run the GMRES optimizer and plot the result (Exercise 2)

python3 main.py -A 8 1 1 3 -b 2 4 -m GMRES -p

### (Additional) Run any optimizer for a 3x3 matrix - visualization is not applicable 

python3 main.py -A 3 1 1 1 3 1 2 1 1 -b 1 2 3 -m GMRES


