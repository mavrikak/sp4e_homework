/*
 * main.cc
 * This is the main file
 */

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "compute_arithmetic.hh"
#include "compute_pi.hh"
#include "riemann_integral.hh"
#include "print_series.hh"
#include "write_series.hh"

int main(int argc, char *argv[]){  

    // input values
    std::stringstream ss;

    // series type, dumper type, frequency, N, precision
    std::string sType, sDumper, freq_str, maxiter_str, precision_str;

    // pointer for series objects
    Series *ptr = NULL;

    // pi or arithmetic series, print or output 
    if ( argc == 6 ) {
        // loading arguments of main
        for (int j = 1; j < argc; j++) ss << argv[j] << " "; 
        ss >> sType >> sDumper >> freq_str >> maxiter_str >> precision_str ; 
        int freq = std::stoi( freq_str );
        int maxiter = std::stoi( maxiter_str ); 
        int precision = std::stoi( precision_str ); 
        
        // arithmetic series
        if (sType == "a") {  
            ptr = new ComputeArithmetic(); 
        }
        // pi series
        else if (sType == "p") {
            ptr = new ComputePi();
        }
        // wrong type of series
        else {    
            std::cerr << "Invalid series type." << std::endl;
            return EXIT_FAILURE;
        }
        // print on screen
        if (sDumper == "p") {
            PrintSeries pseries = PrintSeries(*ptr, freq, maxiter);
            pseries.setPrecision(precision);
            pseries.dump(); 
        }
        // output in .txt
        else if (sDumper == "o") {
            PrintSeries pseries = PrintSeries(*ptr, freq, maxiter);
            pseries.setPrecision(precision);
            std::ofstream outFile("output.txt"); 
            pseries.dump(outFile);
            outFile.close();
        }
        // wrong type of dumper
        else {
            std::cerr << "Invalid dumper." << std::endl;
            return EXIT_FAILURE;
        }

    }
    // pi or arithmetic, write + type of output file
    else if (argc == 7) {
        // type of output file
        std::string fext; 

        // loading arguments of main
        for (int j = 1; j < argc; j++) ss << argv[j] << " "; 
        ss >> sType >> sDumper >> fext >> freq_str >> maxiter_str >> precision_str ; 
        int freq = std::stoi( freq_str );
        int maxiter = std::stoi( maxiter_str ); 
        int precision = std::stoi( precision_str ); 
        
        // arithmetic series
        if (sType == "a") {  
            ptr = new ComputeArithmetic();
        }
        // pi series
        else if (sType == "p") {
            ptr = new ComputePi();
        }
        // invalid type of series
        else {    
            std::cerr << "Invalid series type." << std::endl;
            return EXIT_FAILURE;
        }
        // separator
        std::string sep; 
        if (fext == "csv")  sep = ",";
        else if (fext == "txt")  sep = " ";
        else if (fext == "psv")  sep = "|";
        else {
            std::cerr << "Invalid file extension." << std::endl;
            return EXIT_FAILURE;
        }
        // writing
        if (sDumper == "w") {
            WriteSeries pseries = WriteSeries(*ptr, freq, maxiter);
            pseries.setSeparator(sep);
            pseries.dump(); 
        }
        else {
            std::cerr << "Invalid dumper." << std::endl;
            return EXIT_FAILURE;
        }

    }

    // Riemann sum, print or output file
    else if (argc == 9) {
        // function, a, b
        std::string f_str, a_str, b_str; 

        // loading arguments of main
        for (int j = 1; j < argc; j++) ss << argv[j] << " "; 
        ss >> sType >> f_str >> a_str >> b_str >> sDumper >> freq_str 
        >> maxiter_str >> precision_str; 
        int freq = std::stoi( freq_str );
        int maxiter = std::stoi( maxiter_str ); 
        int precision = std::stoi( precision_str ); 
        double a = std::stod( a_str );
        double b = std::stod( b_str );
            
        std::function<double(double)> func; // integrated f(x)
        // f(x) = x^3
        if (f_str == "xcube")  func = [](double x){ return x * x * x; };
        // f(x) = cos(x)
        else if (f_str == "cosx") func = [](double x){return cos(x);};
        // f(x) = sin(x)
        else if (f_str == "sinx") func = [](double x){return sin(x);};
        // f(x) != x^3 || cos(x) || sin(x)
        else{
            std::cerr << "Invalid function argument." << std::endl;
            return EXIT_FAILURE;
        }
        // Riemann sum
        if (sType == "r") {  
            ptr = new RiemannIntegral(a, b, func);
        }
        else {    
            std::cerr << "Invalid series type." << std::endl;
            return EXIT_FAILURE;
        }
        // printing
        if (sDumper == "p") {
            PrintSeries pseries = PrintSeries( *ptr, freq, maxiter);
            pseries.setPrecision(precision);
            pseries.dump(); 
        }
        // output in .txt file
        else if (sDumper == "o") {
            PrintSeries pseries = PrintSeries( *ptr, freq, maxiter);
            pseries.setPrecision(precision);
            std::ofstream outFile("output.txt"); 
            pseries.dump(outFile);
            outFile.close();
        }
        // Invalid dumper type
        else {
            std::cerr << "Invalid dumper." << std::endl;
            return EXIT_FAILURE;
        }

    }
    // riemann, write + extension
    else if (argc == 10) {
        // function, a, b, output file type
        std::string f_str, a_str, b_str, fext;

        // loading arguments of main
        for (int j = 1; j < argc; j++) ss << argv[j] << " "; 
        ss >> sType >> f_str >> a_str >> b_str >> sDumper >> fext >> freq_str 
        >> maxiter_str >> precision_str ; 
        int freq = std::stoi( freq_str );
        int maxiter = std::stoi( maxiter_str ); 
        int precision = std::stoi( precision_str ); 
        double a = std::stod( a_str );
        double b = std::stod( b_str ); 
            
        std::function<double(double)> func; // integrated f(x)
        // f(x) = x^3
        if (f_str == "xcube")  func = [](double x){ return x * x * x; };
        // f(x) = cos(x)
        else if (f_str == "cosx") func = [](double x){return cos(x);};
        // f(x) = sin(x)
        else if (f_str == "sinx") func = [](double x){return sin(x);};
        // f(x) != x^3 || cos(x) || sin(x)
        else{
            std::cerr << "Invalid function argument." << std::endl;
            return EXIT_FAILURE;
        }
        // Riemann sum
        if (sType == "r") {  
            ptr = new RiemannIntegral(a, b, func);
        }
        else {    
            std::cerr << "Invalid series type." << std::endl;
            return EXIT_FAILURE;
        }
        // separator
        std::string sep; 
        if (fext == "csv")  sep = ",";
        else if (fext == "txt")  sep = " ";
        else if (fext == "psv")  sep = "|";
        else {
            std::cerr << "Invalid file extension." << std::endl;
            return EXIT_FAILURE;
        }
        // writing
        if (sDumper == "w") {
            WriteSeries pseries = WriteSeries(*ptr, freq, maxiter);
            pseries.setSeparator(sep);
            pseries.dump(); 
        }
        else {
            std::cerr << "Invalid dumper." << std::endl;
            return EXIT_FAILURE;
        }

    }
    // any other possibility will raise an error 
    else {
        std::cerr << "Wrong number of arguments." << std::endl
        << "Note: The arguments must be passed in the following order: " <<
        "[series] [dumper] [frequency] [maxiter] [precision] " << std::endl;
        return EXIT_FAILURE;
    }
    delete ptr; 
}
