/* 
 * print_series.hh
 * Creation of the PrintSeries class which inherits from DumperSeries
 * PrintSeries dumps the results on the screen (or other if provided)
 */

#ifndef PRINTSERIES_HPP
#define PRINTSERIES_HPP

#include "dumper_series.hh"

class PrintSeries : public DumperSeries {
    
    private:
        uint f, maxiter, precision;

    public: 
        PrintSeries(Series &series, uint a, uint b);
        ~PrintSeries(); 
        // dumps the result of sum given a std::ostream
        void dump(std::ostream & os = std::cout );
        // provides precision : number of significant digits after comma
        void setPrecision(uint precision);

};

#endif 

