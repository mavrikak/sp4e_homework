/*
 * This file is included in each header file of this project. It
 * contains global variables such as pi.
 */

#include "globals.hh"

const double PI = 3.14159265358979323846264338328;
