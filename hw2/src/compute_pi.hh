/* compute_pi.hh
 * Header file for the ComputePi class
 * The ComputePi class is the class for
 * the computation of pi as a summation
 */

#ifndef COMPUTEPI_HPP
#define COMPUTEPI_HPP

#include "series.hh"

class ComputePi : public Series{
public:
    ComputePi();
    ~ComputePi();

    // functions for pi sum computation
    double compute(uint N);
    double computeTerm(uint k, uint N);

    // gives pi
    double getAnalyticPrediction();
};

#endif
