/* 
 * write_series.hh
 * Creation of WriteSeries class which inherits from DumperSeries class
 * WriteSeries dumps the results in a file of 3 possible formats: txt,csv,psv 
 */

#ifndef WRITESERIES_HPP
#define WRITESERIES_HPP

#include "dumper_series.hh"
#include <string>

class WriteSeries : public DumperSeries {

    private:
        uint f, maxiter; 
        std::string del, type;
    
    public:
        WriteSeries(Series &series, uint a, uint b); 
        ~WriteSeries(); 
        // dumps results in a file
        void dump(std::ostream & os = std::cout);
        // separation of values in the output file
        void setSeparator(std::string sep = " ");
        // set precision (number of digits after the decimal point)
        void setPrecision(uint precision);
        
};

#endif

