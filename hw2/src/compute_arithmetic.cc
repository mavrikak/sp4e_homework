/* compute_arithmetic.cc
 * Implementation of the ComputeArithmetic class
 */

#include "compute_arithmetic.hh"

ComputeArithmetic::ComputeArithmetic() : Series() {}

ComputeArithmetic::~ComputeArithmetic() {}

double ComputeArithmetic::computeTerm(uint k, uint N){
    return 1. * k;
}