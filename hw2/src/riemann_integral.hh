/* riemann_integral.hh
 * Header file for the RiemannIntegral class
 * The RiemannIntegral class is the class for
 * the computation of the integral of a fuction
 * f(x) from x = a to x = b, as the limit of the
 * Riemann sum.
 */

#ifndef RIEMANNINTEGRAL_HPP
#define RIEMANNINTEGRAL_HPP

#include <functional>
#include "series.hh"

class RiemannIntegral : public Series{
private:
    // interval [a,b] of the integral
    double a, b;

    // function to be integrated
    std::function<double(double)> f;

public:
    RiemannIntegral(double inA, double inB,
            std::function<double(double)> infunc);
    ~RiemannIntegral();

    // functions implementing Riemann sum
    double compute(uint N);
    double computeTerm(uint k, uint N);
};

#endif