
# =============================================================================

import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import os.path as osp

# =============================================================================

# use of argparse to input variables from command line
msg = 'Visualization'
parser = ArgumentParser(description=msg, 
                        formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument('-f', '--file', type=str, default="data.txt", metavar='',
                    help='Provide the name of the data file with extension.') 
parser.add_argument('-d', '--directory', type=str, default="../build", 
                    metavar='', help='Provide directory of the data file') 
args = parser.parse_args()

# =============================================================================

def set_separator(fext: str):
    """
    Returns the delimiter given the file format.

    Arguments:
    ----------
    fext : str 
        the file extension 

    Returns:
    --------
    <delimiter>  : str 
        the delimiter
    """ 
    if fext == "txt":   return " "
    elif fext == "csv":     return ","
    elif fext == "psv":     return "|"
    else: 
        raise ValueError("Invalid file extension")

# =============================================================================

if __name__ == "__main__":

    # the delimiter is determined given the file format
    sep = set_separator( fext=args.file.split(".")[-1] )

    # the data are loaded from the data file 
    data = []
    file_path = osp.join( args.directory, args.file )   
    with open( file_path, 'r' ) as f:
        line = f.readline()
        while line:
            data.append( [float(n) for n in line.split(sep)] )
            line = f.readline() 
    data = np.array(data)

    # x : is number of terms N in the sum
    x = data[:,0]
    # y : the value of the sum given N 
    y = data[:,1]
    
    # create figure and axis objects
    fig, ax = plt.subplots() 
    ax.scatter( x, y, label="Series" ) 
    ax.set_xlabel( r"$N$", fontsize=20 )
    ax.set_ylabel( "Value", fontsize=20 )
    ax.tick_params( axis='both', labelsize=18 )
    
    # the following plots the analytical prediction if it is provided
    if data.shape[1] == 3:
        y_pred = data[:,2] 
        ax.scatter( x, y_pred, label="Prediction" )
        ax.legend( fontsize=18 )
    plt.show()

# =============================================================================
