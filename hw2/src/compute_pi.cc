/* compute_pi.cc
 * Implementation of the ComputePi class
 */

#include "compute_pi.hh"

ComputePi::ComputePi() : Series() {}

ComputePi::~ComputePi() {}

// computes pi as a sum
double ComputePi::compute(uint N){
    Series::compute(N);
    return sqrt(6. * curVal);
}

// term of pi's sum
double ComputePi::computeTerm(uint k, uint N){
    return 1. / (double)(k * k);
}

double ComputePi::getAnalyticPrediction(){
    return PI;
}