/* series.cc
 * Implementation of the Series class
 */

#include "series.hh"

Series::Series() : curIndex(0), curVal(0.) {}

Series::~Series() {}

double Series::compute(uint N){
    // check for previous partial summation
    if (curIndex <= N){
        N -= curIndex;
    }
    else{
        curIndex = 0;
        curVal = 0;
    }
    // do the summation for remaining terms
    for (uint i = 0; i < N; i++){
        this->addTerm(N);
    }
    return curVal;
}

// adds each term of the sum
void Series::addTerm(uint N){
    curIndex += 1;
    curVal += this->computeTerm(curIndex, N);
}

// default prediction: NaN
double Series::getAnalyticPrediction(){
    return std::nan("");
}