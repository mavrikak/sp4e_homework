/* 
 * write_series.cc
 * Implementation of methods for the WriteSeries class
 */

#include "series.hh"
#include "write_series.hh" 
#include <cmath>
#include <string>
#include <fstream>
#include <iostream>

WriteSeries::WriteSeries(Series &series, uint a, uint b) 
: DumperSeries(series) {
    f = a; 
    maxiter = b;
};

WriteSeries::~WriteSeries() {};

void WriteSeries::dump(std::ostream & os) {

    // creation of output file 
    std::ofstream outFile("data." + type); 

    // number of evaluations given max iterations and frequency
    uint n_evals = maxiter / f;
    double pred = series.getAnalyticPrediction();
    
    for( uint j = 1; j <= n_evals; j++ ) {
         
        // evaluate sum with current value of N 
        uint N_curr = j * f ;
        double s = series.compute( N_curr ); 
        
        // write in the output file
        outFile << N_curr << del << s; 

        // provide the prediction value if provided
        if ( !std::isnan(pred) ) {
            outFile << del << pred << "\n";  
        }
        else outFile << "\n";

    };
    outFile.close();

};

void WriteSeries::setSeparator(std::string sep) {
    
    // define the file format depending on the delimiter
    if (sep == ",") type = "csv";
    else if (sep == " ")  type = "txt";
    else if (sep == "|")  type = "psv"; 
    else std::cout << "Error\n";
    del = sep; 

};


void WriteSeries::setPrecision(uint precision) {};



