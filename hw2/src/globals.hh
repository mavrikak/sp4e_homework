/*
 * This file is included in each header file of this project. It
 * contains global variables such as pi and global definitions
 * for types like unsigned int, etc.
 */

#ifndef GLOBALS_HPP
#define GLOBALS_HPP

typedef unsigned int uint;

extern const double PI;

#endif
