/* compute_arithmetic.hh
 * Header file for the ComputeArithmetic class
 * The ComputeArithmetic class is the class for
 * the computation of sum(k), for k = 1 to N
 */

#ifndef COMPUTEARITHMETIC_HPP
#define COMPUTEARITHMETIC_HPP

#include "series.hh"

class ComputeArithmetic : public Series{
public:
    ComputeArithmetic();
    ~ComputeArithmetic();

    // term for sum of this class
    double computeTerm(uint k, uint N);
};

#endif