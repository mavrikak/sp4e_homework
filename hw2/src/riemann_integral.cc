/* riemann_integral.cc
 * Implementation of the RiemannIntegral class
 */

#include <iostream>
#include "riemann_integral.hh"

RiemannIntegral::RiemannIntegral(double inA, double inB,
                        std::function<double(double)> infunc)
    : Series(), a(inA), b(inB), f(infunc) {}

RiemannIntegral::~RiemannIntegral() {}

// Riemann sum
double RiemannIntegral::compute(uint N){
    curIndex = 0;
    curVal = 0;
    for (uint i = 0; i < N; i++){
        this->addTerm(N);
    }
    return curVal;
}

// Riemann sum terms
double RiemannIntegral::computeTerm(uint k, uint N){
    double dx = (double)((b - a) / N);  // summands
    double x_k = a + k * dx;            // chosen x in [x(i-1),x(i)]
    return this->f(x_k) * dx;           // sum term: f(x_k)*dx
}