/* 
 * print_series.cc
 * Implementation of methods for the PrintSeries class
 */


#include "series.hh"
#include "print_series.hh"
#include <iostream> 
#include <cmath>
#include <iomanip>

PrintSeries::PrintSeries(Series &series, uint a, uint b) 
: DumperSeries(series) {
    f = a; 
    maxiter = b;
};

PrintSeries::~PrintSeries() {}; 

void PrintSeries::dump(std::ostream & os) {
    
    // number of evaluations based on the max iterations and frequency
    uint n_evals = maxiter / f; 
    // loads the analytical prediction  
    double pred = series.getAnalyticPrediction();

    for( uint j = 1; j <= n_evals; j++ ) {
        
        // current value of N for the sum
        uint N_curr = j * f ;
        // compute the series for given N_curr
        double s = series.compute( N_curr ); 
        
        // prints the result for current N
        os << "Sum for N = " << N_curr << " : " << std::fixed 
        << std::setprecision(precision) << s;

        // evaluates the deviation from the analytical prediction ...
        // if it is provided, i.e. is not NaN
        if ( !std::isnan(pred) ) {
            double diff = pred - s;     
            os << " with deviation of " << diff << std::endl; 
        }
        else os << std::endl;

    };

};

void PrintSeries::setPrecision(uint precision) {
    this->precision = precision;            
};

