/* 
 * dumper_series.hh
 * Definition of DumperSeries class as a mother class
 */

#ifndef DUMPERSERIES_HPP
#define DUMPERSERIES_HPP

#include "series.hh"
#include <iostream> 

class DumperSeries{

    public:
        DumperSeries(Series & in_series);
        virtual ~DumperSeries(); 
        virtual void dump(std::ostream & os) = 0; 
        virtual void setPrecision(uint precision) = 0;
   
    protected:
        Series & series;

};

// operation overload for the << operator 
// to allow for writing in an output file
inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) 
{ 
    _this.dump(stream);
    return stream;
};
 
#endif 

