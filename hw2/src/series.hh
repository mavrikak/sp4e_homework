/* series.hh
 * Header file for the Series class
 * The Series class is the interface for the family
 * of classes for computing different types of series
 */

#ifndef SERIES_HPP
#define SERIES_HPP

#include <cmath>
#include "globals.hh"

class Series{
protected:
    uint curIndex;  // keeps sum index for Dumper
    double curVal;  // keeps sum value for Dumper
    
public:
    Series();
    virtual ~Series();
    
    // functions for sum computation
    virtual double compute(uint N);
    void addTerm(uint N);
    virtual double computeTerm(uint k, uint N) = 0;

    // function for analytic sum prediction
    virtual double getAnalyticPrediction();
};

#endif