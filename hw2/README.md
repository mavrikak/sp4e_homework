# C++ Classes (Assignment 2)

## PhD Students 

Parmenion Mavrikakis - Nanophotonics and Metrology Laboratory, EPFL  
Stavros Athanasiou - Nanophotonics and Metrology Laboratory, EPFL 

## Dependencies 

| C++ libraries | Python libraries |
| ----------- | ----------- |
| cmath | numpy |
| iostream | argparse |
| iomanip | matplotlib.pyplot |
| functional | os.path |
| string | - |
| fstream | - |
| sstream | - |

## Structure 

| File Names | Description |
| ----------- | ----------- |
| main.cc | main function |
| globals.hh, globals.cc | Contains global variables and global definitions |
| series.hh, series.cc | Abstract class for other classes that compute different types of series |
| compute_arithmetic.hh, compute_arithmetic.cc | Class for the computation of Sum(k), for k = 1 to N |
| compute_pi.hh, compute_pi.cc | Class for the computation of pi in the form of series |
| riemann_integral.hh, riemann_integral.cc | Class for the computation of integrals with Riemann sum |
| dumper_series.hh, dumper_series.cc | Abstract class for other classes that print/output numerical results |
| print_series.hh, print_series.cc | Class for printing (and writing) numerical results |
| write_series.hh, write_series.cc | Class for  writing numerical results |
| plot_data.py | File for reading and plotting numerical results |

## How to run 
The following segment provides instructions on how to execute the provided code.

### Command line arguments
For each of the following arguments, the different options are provided.
For some of these options, further arguments are required to be provided.
Examples will be presented in the next subsection. 

* seriesType:
    * a (string): Computation of the arithmetic series, for k = 1 to N
    * p (string): Computation of pi in the form of series
    * r (string): Computation of integrals with Riemann sum
        * func (string): Function to integrate (provide one of the following)
            * xcube (string): x in third power 
            * cosx (string): cosine of x 
            * sinx (string): sine of x 
        * a (double): Lower boundary of integration interval [a, b]
        * b (double): Upper boundary of integration interval [a, b]
* seriesDumper:
    * p (string): Printing of numerical results on the screen
    * o (string): Writing of numerical results in a output.txt file
    * w (string): Writing of numerical results chosen file format
        * txt (string): Uses space/tab as delimeter ( )  
        * csv (string): Uses comma as delimeter (,) 
        * psv (string): Uses pipe as delimeter (|) 
* freq (int): Sum printing frequency
* N (int): Upper limit of sum
* prec (int): number of decimal points for numerical results

### Running information

* Make a build folder
* In the build folder:
    * cmake ..
    * make
    * run the following commands

### Evaluation of the Arithmetic series
* Print numerical results on the screen: ./src/main a p freq N prec
    * example: ./src/main a p 10 1000 3
* Output numerical results (.txt): ./src/main a o freq N prec
    * example: ./src/main a o 10 1000 3
* Write numerical results to chosen file type: ./src/main a w fileFormat freq N prec
    * example: ./src/main a w txt 10 1000 3
    * example: ./src/main a w csv 10 1000 3
    * example: ./src/main a w psv 10 1000 3

### Evaluation of series for pi 
* Print numerical results on the screen: ./src/main p p freq N prec
    * example: ./src/main p p 10 1000 3
* Output numerical results (.txt): ./src/main p o freq N prec
    * example: ./src/main p o 10 1000 3
* Write numerical results to chosen file type: ./src/main p w fileFormat freq N prec
    * example: ./src/main p w txt 10 1000 3
    * example: ./src/main p w csv 10 1000 3
    * example: ./src/main p w psv 10 1000 3

### Integration of functions using Riemann sum 
* Print numerical results on the screen: ./src/main r func a b p freq N prec
    * x cube: ./src/main r xcube 0 1 p 10 1000 3
    * cos(x): ./src/main r cosx 0 3.14159265 p 10 1000 3
    * sin(x): ./src/main r sinx 0 1.57079633 p 10 1000 3
* Output numerical results (.txt): ./src/main r func a b o freq N prec
    * x cube: ./src/main r xcube 0 1 o 10 1000 3
    * cos(x): ./src/main r cosx 0 3.14159265 o 10 1000 3
    * sin(x): ./src/main r sinx 0 1.57079633 o 10 1000 3
* Write numerical results to chosen file type: ./src/main r func a b w fileFormat freq N prec
    * txt files:
        * x cube: ./src/main r xcube 0 1 w txt 10 1000 3
        * cos(x): ./src/main r cosx 0 3.14159265 w txt 10 1000 3
        * sin(x): ./src/main r sinx 0 1.57079633 w txt 10 1000 3
    * csv files:
        * x cube: ./src/main r xcube 0 1 w csv 10 1000 3
        * cos(x): ./src/main r cosx 0 3.14159265 w csv 10 1000 3
        * sin(x): ./src/main r sinx 0 1.57079633 w csv 10 1000 3 
    * psv files:
        * x cube: ./src/main r xcube 0 1 w psv 10 1000 3
        * cos(x): ./src/main r cosx 0 3.14159265 w psv 10 1000 3
        * sin(x): ./src/main r sinx 0 1.57079633 w psv 10 1000 3 

### Run Python Plot
A Python script is provided for plotting the numerical results. It uses 
argparse to provide:
* The name of the data file (default: data.txt)
* The directory at which is located (default: ../build) 

Format: python3 plot_data.py -f <filename> -d <directory> 
Example: python3 plot_data.py -f data.csv -d ../build 

Note that the WriteSeries class by default saves the data in a file named
"data.<extension>".  

## Answers to questions in exercises

* Exercise 2.1: The exercises were divided so that each member could implement codes that do not correlate heavily.
    * Parmenion: Exercises 2, 5 and 6 -> Exercises containing Series Class and the Classes that inherit from it.
    * Stavros: Exercises 3 and 4 -> Exercises containing DumperSeries Class and the Classes that inherit from it.

* Exercise 5.1: Complexity = O((N/f)*N) = O(N * N), where f = frequency, since PrintSeries runs function compute ~ O(N), in a for loop of N/f iterations.

* Exercise 5.4: Complexity = O((N/f)*f) = O(N), where f = frequency, because PrintSeries runs function compute ~ O(f) in a for loop of N/f iterations.

* Exercise 5.5: In the case of summing terms reversely in order to reduce the rounding errors over floating point operation, the complexity remains the same in both of the previous cases. Since, the for loop has the same number of iterations, its' complexity remains unchanged.

* Exercise 6.4: For every function we get values that are close to the analytical ones.
    * x cube: For N = 101 and precision = 2 we get the value 0.25
    * cos(x): For N = 629 and precision = 2 we get the value -0.00
    * sin(x): For N = 157 and precision = 2 we get the value 1.00

