#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include "compute.hh"
#include "compute_gravity.hh"
#include "compute_interaction.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

class PyParticlesFactoryInterface : public ParticlesFactoryInterface {
public:
    // Inherit the constructors
    using ParticlesFactoryInterface::ParticlesFactoryInterface;

    // Trampoline
    SystemEvolution& createSimulation(const std::string& fname,
                                    Real timestep) override {
    PYBIND11_OVERRIDE_PURE(
        SystemEvolution&,           // Return type
        ParticlesFactoryInterface,  // Parent class
        createSimulation,           // Name of function in C++
        fname, timestep             // Argument(s)
    );
    }
};

PYBIND11_MODULE(pypart, m) {

    m.doc() = "pybind for the Particles project";

    // binding for System class
    py::class_<System>(m, "System");

    // binding for SystemEvolution class
    py::class_<SystemEvolution>(m, "SystemEvolution")
        .def("evolve", &SystemEvolution::evolve, "Evolution of simulation")
        .def("addCompute", &SystemEvolution::addCompute,
            "Addition of computation")
        .def("getSystem", &SystemEvolution::getSystem,
            py::return_value_policy::reference, "Get system of particles")
        .def("setNSteps", &SystemEvolution::setNSteps, "Set number os steps")
        .def("setDumpFreq", &SystemEvolution::setDumpFreq,
            "Set frequency for dumps");

    // binding for ParticlesFactoryInterface class
    py::class_<ParticlesFactoryInterface, PyParticlesFactoryInterface>(
        m, "ParticlesFactoryInterface")
        .def("createSimulation",
            static_cast<SystemEvolution& (
                ParticlesFactoryInterface::*)(const std::string&, Real)>(
                &ParticlesFactoryInterface::createSimulation),
            py::return_value_policy::reference,
            "Abstract function for creating a simulation")
        .def("createSimulation",
            static_cast<SystemEvolution& (
                ParticlesFactoryInterface::*)(const std::string&, Real,
                                                std::function<void(
                                                    ParticlesFactoryInterface&,
                                                    Real)>)>(
                &ParticlesFactoryInterface::createSimulation),
            py::return_value_policy::reference,
            "Overriden function for creating a simulation")
        .def_static("getInstance", &ParticlesFactoryInterface::getInstance,
                    py::return_value_policy::reference,
                    "Get particle's factory instance (singleton)")
        .def_property_readonly("system_evolution",
                                &ParticlesFactoryInterface::getSystemEvolution,
                                "Get system evolution object");

    // binding for PlanetsFactory class
    py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
        .def_static("getInstance", &PlanetsFactory::getInstance,
                    py::return_value_policy::reference,
                    "Get planet's factory instance (singleton)");

    // binding for PingPongBallsFactory class
    py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(
        m, "PingPongBallsFactory")
        .def_static("getInstance", &PingPongBallsFactory::getInstance,
                    py::return_value_policy::reference,
                    "Get ping-pong balls' factory instance (singleton)");

    // binding for MaterialPointsFactory class
    py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(
        m, "MaterialPointsFactory")
        .def_static("getInstance", &MaterialPointsFactory::getInstance,
                    py::return_value_policy::reference,
                    "Get material point's factory instance (singleton)");

    // binding for Compute mother class
    py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute")
        .def("compute", &Compute::compute);

    // binding for ComputeInteraction daughter class
    py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(
        m, "ComputeInteraction")
        .def("applyOnPairs", &ComputeInteraction::applyOnPairs<py::function>);

    // binding for ComputeGravity daughter class
    py::class_<ComputeGravity, ComputeInteraction,
                std::shared_ptr<ComputeGravity>>(m, "ComputeGravity")
        .def(py::init<>())
        .def("compute", &ComputeGravity::compute)
        .def("setG", &ComputeGravity::setG, py::arg("G"),
            "Set the value of G (Real)");

    // binding for ComputeTemperature daughter class
    py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(
        m, "ComputeTemperature")
        .def(py::init<>())
        .def("compute", &ComputeTemperature::compute)
        .def_property("conductivity", &ComputeTemperature::getConductivity,
                    &ComputeTemperature::setConductivity)
        .def_property("capacity", &ComputeTemperature::getCapacity,
                    &ComputeTemperature::setCapacity)
        .def_property("density", &ComputeTemperature::getDensity,
                    &ComputeTemperature::setDensity)
        .def_property("L", &ComputeTemperature::getL, &ComputeTemperature::setL)
        .def_property("deltat", &ComputeTemperature::getDeltat,
                    &ComputeTemperature::setDeltat);

    // binding for ComputeVerletIntegration daughter class
    py::class_<ComputeVerletIntegration, Compute,
                std::shared_ptr<ComputeVerletIntegration>>(
        m, "ComputeVerletIntegration")
        .def(py::init<Real>(), py::arg("timestep"))
        .def("compute", &ComputeVerletIntegration::compute)
        .def("setDeltaT", &ComputeVerletIntegration::setDeltaT, py::arg("dt"),
            "Set the value of deltaT")
        .def("addInteraction", &ComputeVerletIntegration::addInteraction);

    // binding for CvsWrite daughter class
    py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(m, "CsvWriter")
        .def(py::init<const py::str&>())
        .def("write", &CsvWriter::write)
        .def("compute", &CsvWriter::compute);

}

