#!/bin/env python3

# =============================================================================

import numpy as np
from error import readPositions, computeError 
from main import main
from optim import optimizeVelocity, readAndPlot 

# =============================================================================

if __name__ == "__main__":

    # run a simulation with init.csv 
    main( nsteps=365, freq=1, filename='init.csv', particle_type='planet',
          timestep=1 )

    directory_ref = "./trajectories/"
    directory_sim = "./dumps/"
    
    # compute error from generated trajectories
    planets = ['sun','mercury','venus','neptune','jupiter','mars','earth',
               'saturn','uranus']
    for planet_name in planets:
        positions_ref = readPositions( planet_name, directory_ref )
        positions = readPositions( planet_name, directory_sim )
        error_result = computeError( positions, positions_ref )
        print(f"Integral Error E(p) for {planet_name}:", error_result)

    # optimization for proper scaling for Mercury
    optimizeVelocity( s0=1.0, filename='mercury_scaling_optim.txt' )
    readAndPlot( filename='mercury_scaling_optim.txt' )

# =============================================================================
