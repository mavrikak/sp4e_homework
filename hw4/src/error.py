# =============================================================================

import numpy as np
import os

# =============================================================================

def readPositions(planet_name: str, directory: str):
    """
    Reads the trajectory of a given planet and makes a numpy array out of it. 

    Arguments: 
    ----------
    planet_name: str 
        the name of the planet 
    directory: str 
        the name of the directory of the planets' files 

    """

    planet_positions = []

    # Iterate through each file in the folder
    for file_name in sorted( os.listdir(directory) ):
        file_path = os.path.join(directory, file_name)

        # Read lines from the file
        with open(file_path, 'r') as file:
            for line in file:
                values = line.strip().split()
                if values[-1] == planet_name:
                    # Extract the position coordinates
                    coordinates = list(map(float, values[:3]))
                    planet_positions.append(coordinates)

    # Trajectory numpy array
    planet_array = np.array(planet_positions)
    return planet_array

# =============================================================================

def computeError(positions: np.ndarray, positions_ref: np.ndarray):
    """
    Computes the error between two trajectories.

    Arguments:
    ----------
    positions: numpy.ndarray
        the trajectory of the planet 
    positions_ref: numpy.ndarray
        the reference trajectory of the planet

    """
    if positions_ref.shape != positions.shape:
        print("References positions matrix shape: ", positions_ref.shape)
        print("Computed positions matrix shape: ", positions.shape)
        raise ValueError("Trajectories must have the same shape")

    # Calculate the integral error
    pos_diff = np.linalg.norm(positions_ref - positions, axis=1)
    error = np.sqrt(np.sum(pos_diff**2))
    return error

# =============================================================================

