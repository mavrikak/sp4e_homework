# =============================================================================

import numpy as np
import scipy, os
from main import main
from error import readPositions, computeError
import matplotlib.pyplot as plt

# =============================================================================

def generateInput( scale: int, planet_name: str, input_filename: str, 
                   output_filename: str ):
    """
    Generates new input with scaled velocity for a given planet. 

    Arguments:
    ----------
    scale: float 
        the scaling for the velocity of a given planet 
    planet_name: str 
        the name of the planet 
    input_filename: str 
        the name of the initial data file
    output_filename: str 
        the name of the final (new) data file

    """

    with open( input_filename, 'r') as f:

        dat, planets = [], []
        line = f.readline()
        while line:
            if "#" in line:
                labels = line
            else:
                data = line.strip().split(' ') 
                vals = [float(j) for j in data[:-1]]
                planet = data[-1]
                dat.append( vals )
                planets.append( planet ) 
            line = f.readline()
            
    dat = np.array(dat)

    if planet_name not in planets:
        msg = f"Planet {planet_name} is not included in the list of planets"
        raise ValueError( msg )

    ind = planets.index( planet_name )
    dat[ind,3:6] = dat[ind,3:6] * scale

    with open( output_filename, 'w' ) as f:
        f.write( labels )
        for i in range( len(planets) ):
            f.write( ' '.join( [f'{j}' for j in dat[i]] ) )      
            f.write( ' ' + planets[i] + '\n')

# =============================================================================

def launchParticles( input: str, nb_steps: int, freq: int):
    """
    Launches the particle codes from main.py for planets and timestep=1 (1
    day). 

    Arguments:
    ----------
    input: str 
        the name of the initial data file
    nb_steps: int
        number of steps in the simulation of evolution
    freq: int
        the frequency for dumping
    """
    main(nb_steps, freq, input, particle_type='planet', timestep=1)

# =============================================================================

def runAndComputeError( scale: float, planet_name: str, input: str, 
                        nb_steps: int, freq: int):
    """
    Runs the simulation and computes the error function for a given planet. 

    Arguments:
    ----------
    scale: float 
        the scaling for the velocity of a given planet 
    planet_name: str 
        the name of the planet 
    input: str 
        the name of the initial data file
    nb_steps: int
        number of steps in the simulation of evolution
    freq: int
        the frequency for dumping

    Returns:
    --------
    err : float 
        the value of the error function 
    """

    fname, fext = input.split('.')
    output_filename = fname + f'_vscale_{planet_name}.' + fext 
    generateInput( scale, planet_name, input, output_filename)
    launchParticles( output_filename, nb_steps, freq)
    Xref = readPositions( planet_name, 'trajectories' )
    X = readPositions( planet_name, 'dumps' )    
    err = computeError( X, Xref )
    return err 

# =============================================================================

def optimizeVelocity( s0: float = 1.0, filename: str = '' ):
    """
    Finds the optimal scaling for the velocity of Mercury. 

    Arguments:
    ----------
    s0 : float      (default: 1.0) 
        the initial scaling
    filename : str 
        the name of file to save data 
    """

    data = [] 
    
    # lambda function to provide to optimizer
    f = lambda s: runAndComputeError( s, planet_name='mercury', 
                                      input='init.csv', nb_steps=365, freq=1)    

    data.append( [s0, f(s0)] )
    def callback(x):
        """
        This function collects the intermediate values of x and f(x) until
        convergence.

        Arguments:
        ----------
        x : np.array
            the intermediate values obtained from optimization

        """
        data.append( [x.tolist()[0], f(x).tolist()] )

    print("Optimizing...")
    scipy.optimize.fmin( f, s0, callback=callback, ftol=0.1 )
    with open( filename, 'w') as f:
        for d in data:
            f.write(f'{d[0]} {d[1]}\n')  
    
    print(f"Optimal scaling is {data[-1][0]}")

# =============================================================================
    
def readAndPlot( filename: str ):
    """
    Reads the generated data from the optimization method and plots them.

    Arguments:
    ----------
    filename : str 
        the name of data file

    """

    data = []
    with open(filename, 'r') as f:
        for line in f:
            x, f = line.strip().split(' ')
            data.append( [float(x),float(f)] )
    data = np.array(data)
    x, y = data[:,0], data[:,1]

    fig = plt.figure(figsize=(9, 7))
    ax = fig.add_axes([0.1, 0.1, 0.85, 0.85])
    ax1 = fig.add_axes([0.6, 0.2, 0.25, 0.25])

    ax.plot( data[:,0], data[:,1], '-bo' )
    ax.set_xlabel( r'Scale Factor', fontsize=15 )
    ax.set_ylabel( r'Error', fontsize=15 )
    ax.set_yscale( 'log' )
    ax.grid() 

    ax.quiver( x[:-1], y[:-1], x[1:]-x[:-1], y[1:]-y[:-1], scale_units='xy',
               angles='xy', scale=1, headlength=4, headwidth=2, width=0.005)

    ax1.plot( data[:,0], data[:,1], '-bo' )
    ax1.set_xlabel( r'Scale Factor', fontsize=12 )
    ax1.set_ylabel( r'Error', fontsize=12 )

    xls = np.linspace( 0.398, 0.40, 4 )
    ax1.set_xlim( 0.398,0.40 )
    ax1.set_xticks( ticks=xls, labels=[f'{j:.4f}' for j in xls])

    yls = np.linspace( 0.51, 0.58, 4 )
    ax1.set_ylim( 0.51,0.58 ) 
    ax1.set_yticks( ticks=yls, labels=[f'{j:.4f}' for j in yls])

    ax1.set_yscale( 'log')
    ax1.quiver( x[:-1], y[:-1], x[1:]-x[:-1], y[1:]-y[:-1], scale_units='xy',
                angles='xy', scale=1, headlength=5, headwidth=3, width=0.02)

    plt.show()

# =============================================================================

