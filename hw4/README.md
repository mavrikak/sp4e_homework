# Trajectory Optimization (Assignment 4)

## PhD Students 

Parmenion Mavrikakis - Nanophotonics and Metrology Laboratory, EPFL  
Stavros Athanasiou - Nanophotonics and Metrology Laboratory, EPFL 

## How to Get Started

### build/ directory

First step is to create a `build/` directory in `hw4/`: 

```console 
$ mkdir build/
```

Create `dumps/` directory.

```console 
$ mkdir src/dumps/
```

### Dependencies 

For google test, run: 
  
```console
$ git submodule update --init googletest/
```

For pybind11, run: 

```console
$ git submodule update --init pybind11/ 
```

For linear algebra C++ library, run:  

```console
$ git submodule update --init eigen/
```

## How to Run

Start by cmaking within `build/`.

```console
$ cmake ..
$ make
```

Because the target pypart is not built by the above procedure, which leads to a `ModuleNotFoundError` later. Thus, in `build/` you should run again the following
commands.

```console
$ cmake ..
$ make
```

All subsequent runs should take place within `hw4/src/`. 

To run particle's code solely, run the following command,

```console
$ python3 main.py 365 1 init.csv planet 1 
```

which saves the generated data in `dumps/` inside `hw4/src/`. 

Script `main2.py` includes running the particle's code and identifying the 
error in the trajectories of Mercury (prints the error for all planets).
Next follows the optimization in which the optimal scaling is found for the 
velocity of Mercury such that it minimizes the error function for the 
trajectories. 
A plot is generated displaying the evolution of the error as a function of the
scaling, in which the optimal scaling is indicated.
To run `main2.py`, type

```console
$ python3 main2.py 
``` 

To increase the precision of the generated trajectories, use the following 
in `main2.py`.

```python
main( nsteps=730, freq=2, filename='init.csv', particle_type='planet', 
      timestep=0.5 )
```

## Overload of createSimulation function

The `ParticlesFactoryInterface` class contains a `std::function` member, that takes a `Real` argument and returns `void`, called `createComputes`. Also, it has a pure virtual method called `createSimulation`, which is overloaded with a template method, that takes a functor as an additional third argument. In the template method `createSimulation` a lambda function, that captures all external variables by reference and takes the `timestep` as an argument, is passed to `createComputes`. The main body of the lambda function contains only the input functor, that takes a `ParticlesFactoryInterface` reference and `timestep` as arguments. Then, the original version of `createSimulation` is used. Hence, this template gives the opportunity to use custom made new functions for creating the computes needed by the code, without making any changes in the methods that have already been implemented.
