# sp4e Assignments

## Description 
This repository includes our attempts on the assignments provided in the 
PhD course "Scientific Programming For Engineers". 

## Getting started 

To run this repository, make sure to have the following packages:

 - Python  
 - cmake
 - C++14 

## Authors
- Parmenion Mavrikakis - Nanophotonics and Metrology Laboratory, EPFL  
- Stavros Athanasiou - Nanophotonics and Metrology Laboratory, EPFL 


